﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WWW_GetPost : MonoBehaviour {

    public string _url = "http://www.google.com";

    public string Url
    {
        get { return _url; }
        set {
            if (!Uri.IsWellFormedUriString(value, UriKind.Absolute))
            Debug.LogWarning("The URI is not well formed !!",this);
                _url = value;
        }
    }
    public List<GET_POST> _fields;

    public List<GET_POST> Fields
    {
        get { return _fields; }
        set { _fields = value; }
    }


    public bool _loading;
    public bool Loading
    {
         get { return _loading; }
        private set { _loading = value; }
    }

    private string textLoaded = "";

    public string TextLoaded
    {
        get { return textLoaded; }
        private  set { textLoaded = value; }
    }
    private string error = "";

    public string Error
    {
        get { return error; }
        private set { error = value; }
    }

    public WWWForm GetWWWForm()
    {
        WWWForm POST = new WWWForm();
        int iPost = 0;
        foreach (GET_POST gp in _fields)
            if (gp.type == GET_POST.SendType.POST)
            {
                iPost++;
                POST.AddField(gp.key, gp.value);
            }
        return iPost>0? POST:null;
    }
    public string GetUrl(bool url=true, bool getParams=true)
    {
        int iGet = 0;
        string GET = "";
        foreach (GET_POST gp in _fields)
            if ( gp.type == GET_POST.SendType.GET)
            {
                iGet++;
                GET += gp.key + "=" + gp.value + "&";
            }
        if (url && getParams)
            return this.Url + (iGet > 0 ? "?"+GET : "");
        else if (url)
            return this.Url;
        else
            return GET;
    }

    public WWW GetWWW(bool withPost,bool withGet)
    {
        string url = GetUrl(true,withGet);
        WWWForm form =  GetWWWForm();
        WWW www = withPost && form!=null ?
            new WWW(url, form) : new WWW(url);
        return www;
    }
    public void LoadWithCoroutine(bool withPost = true, bool withGet = true) 
    {
        StartCoroutine(Load(withPost, withGet));
    }

    public IEnumerator Load(bool withPost=true, bool withGet=true) 
    {

        WWW www = GetWWW(withPost, withGet);
        string url = www.url;
        GET_POST[] getAndPost = this.Fields.ToArray();
        Loading = true;
        if (onStartPageLoading != null)
            onStartPageLoading(url, getAndPost);

        yield return www;

            Loading = false;
            Error = www.error;
            TextLoaded = www.text;
            
        if (onEndPageLoading != null)
            onEndPageLoading(url, getAndPost,textLoaded,error);
        
        yield return null;
    }

	[System.Serializable]
    public class GET_POST {
        public string key="key";
        public string value="value";
        public enum SendType {GET,POST }
        public SendType type=SendType.POST;
        public GET_POST(string key, string value, SendType sendType)
        {
            this.key = key;
            this.value = value;
            this.type = sendType;
        }
    }

    public void SetParams(string key, string value) 
    {
        foreach (GET_POST gp in _fields) 
        {
            if (gp.key.Equals(key))
                gp.value = value;
        }
    
    }
    public void SetParams(string key, GET_POST.SendType sendType)
    {
        foreach (GET_POST gp in _fields)
        {
            if (gp.key.Equals(key))
                gp.type = sendType;
        }

    }
    public void SetParams(string key, string value, GET_POST.SendType sendType)
    {
        foreach (GET_POST gp in _fields)
        {
            if (gp.key.Equals(key)) { 
                gp.value = value;
                gp.type = sendType;}
        }

    }

    public void AddParams(string key, string value, GET_POST.SendType sendType) 
    {
        Fields.Add(new GET_POST(key, value, sendType));
    }

    public delegate void OnStartPageLoading(string urlSent,GET_POST [] paramsSent);
    public delegate void OnEndPageLoading(string urlSent, GET_POST [] paramsSent, string textReceived, string error);

    public OnStartPageLoading onStartPageLoading;
    public OnEndPageLoading onEndPageLoading;

}
