﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Store  {



	public static int create, find, hashTab;
	static IDictionary<string,GameObject> alreadyFound = new Dictionary<string,GameObject>();

	public static bool In(string name, GameObject obj)
	{
		if(obj!=null){
			GameObject father=null;
			//Do the parent is already ref. in the dictonary -> complexity O(1)
				bool isInHashTab;
					isInHashTab = alreadyFound.TryGetValue(name,out father);

				//If the object has been delete, catch the exception and hanlde it by removinf from the hash table
				
			//Debug.Log(isInHashTab);
			if(isInHashTab && father==null)
			{
				alreadyFound.Remove(name);
				//Debug.Log("Remove");
			}
			if(isInHashTab && father!=null)
			{
				obj.transform.parent=father.transform;
				hashTab++;
				//	Debug.Log ("Store in hashtable: "+ father.name);

			}
			//Damn it
			else {

				//do the parent existe in the scene ?
				father = GameObject.Find(name) as GameObject;
				find++;
				if( father!=null)
				{
					obj.transform.parent=father.transform;
				
				//		Debug.Log ("Store in a found object: "+father.name);
				}
				//Ok, none existe, we are going to create the father
				else
				{
					father = new GameObject(name);
					obj.transform.parent=father.transform;
					create++;
						
				//		Debug.Log ("Create the parent:" + father.name);
				}
				//if a father is found, store it in the dictonary to do not lost time to found it next time
				try{
					if(father!=null){alreadyFound.Add(name, father);}
				}catch(ArgumentException)
				{//Debug.Log("Already Existi in");
				}
			
		}

			return true;
		}
		return false;
	}

	public  static  string GetString()
	{
		return "C: "+create +",F: "+find +", H: "+hashTab;
	}
}
