﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */



public enum MenuPositionType  {

	TopLeftCorner, TopRightCorner, BotLeftCorner, BotRightCorner, Center
}

public enum MenuMesureType
{

	PX, PCT
}